const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

console.log(socket.id); // undefined

socket.on("connect", () => {
	console.log(socket.id); // "G5p5..."
	console.log(`${socket.id} connected`);
	socket.on("disconnect", () => {
		console.log(`${socket.id} disconected`);
	});
});