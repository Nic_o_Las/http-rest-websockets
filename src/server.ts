import express from 'express';
import http from 'http';
import path from "path";
import { Server } from 'socket.io';
import socketHandler from './socket';
import routes from './routes';
import { HTML_FILES_PATH, STATIC_PATH, PORT } from './config';

const app = express();
const httpServer = new http.Server(app);
const socketIo = new Server(httpServer);

app.use(express.static(STATIC_PATH));

routes(app);

app.get('*', (req, res) => {
	res.redirect('/login');
	res.sendFile(path.join(HTML_FILES_PATH, "notFound.html"));
});

socketHandler(socketIo);

httpServer.listen(PORT, () => {
	console.log(`Listen server on port ${PORT}`);
});

export default { app, httpServer };
